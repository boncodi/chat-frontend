import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '@/store'
import Index from '@/views/IndexPage'
import ChatPage from '@/views/chat/ChatRoom'
import SignIn from '@/views/auth/SignIn'
import RootLayout from "@/components/RootLayout";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    scrollBehavior: function (to, from, savedPosition) {
        if (to.hash) {
            return {
                selector: to.hash
            }
        }
        if (from && to.name === from.name && to.params === from.params) {
            return
        }
        return savedPosition || {x: 0, y: 0}
    },
    routes: [
        {
            path: '/',
            component: RootLayout,
            children: [
                {path: '/', component: Index, meta: {requireAuth: true}},
                {path: '/chat/:room_uuid/:user_id', name: 'chat-page', component: ChatPage, meta: {requireAuth: true}},
            ]
        },
        {path: '/login', name: 'auth-login', component: SignIn},
        {path: '*', redirect: '/'}
    ],
})

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token')
    const hasMe = store.state.auth.me
    if (to.matched.some(m => m.meta.requireAuth)) {
        if (token && !hasMe && to.name !== 'auth-login') {
            return store.dispatch('auth/getMe').then(() => {
                next()
            })
        }
        if (!token) {
            return next({name: 'auth-login'})
        }
    }
    next()
})

export default router
