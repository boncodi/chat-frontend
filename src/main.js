import Vue from 'vue'
import App from './App.vue'
import '@/assets/scss/main.css'

import '@/plugins/vee-validate'
import '@/plugins/axios'
import '@/plugins/vue-chat-scroll'
import router from '@/router'
import store from '@/store'

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
