export const API_HOST = process.env.VUE_APP_API_BASE_URL || 'http://127.0.0.1:8000'

export const API_VERSION = 'v1'
export const API_URL = `${API_HOST}/api/${API_VERSION}`

export const WS_HOST = process.env.VUE_APP_WS_BASE_URL || 'ws://127.0.0.1:8000'
export const WS_URL = `${WS_HOST}/api/${API_VERSION}`