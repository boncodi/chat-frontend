import axios from '@/plugins/axios'
/**
 * States
 */
const state = {
    me: null,
}

/**
 * Getters
 */
const getters = {}

/**
 * Actions
 */
const actions = {
    getMe({ commit }) {
        return axios.get('/auth/me').then(response => {
            commit('SET_ME', response?.data)
        })
    },
    login({ dispatch }, data) {
        return axios.post('/auth/sign-in', data).then(response => {
            dispatch('chat/createRoom', null, { root: true })
            localStorage.setItem('token', response?.data?.access_token)
        })
    },
    logout({ commit }) {
        commit('SET_ME', null)
        localStorage.removeItem('token')
    }
}

/**
 * Mutations
 */
const mutations = {
    ['SET_ME'](state, data) {
        state.me = data
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
