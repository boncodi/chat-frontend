import axios from '@/plugins/axios'
/**
 * States
 */
const state = {
    room: JSON.parse(localStorage.getItem('room') || 'null'),
}

/**
 * Getters
 */
const getters = {}

/**
 * Actions
 */
const actions = {
    createRoom({ commit }) {
        return axios.post('/chat/rooms/').then(response => {
            localStorage.setItem('room', JSON.stringify(response?.data))
            commit('SET_ROOM', response?.data)
        })
    },
}

/**
 * Mutations
 */
const mutations = {
    ['SET_ROOM'](state, data) {
        state.room = JSON.stringify(data)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
