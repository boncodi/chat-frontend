import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import auth from '@/store/modules/auth'
import chat from '@/store/modules/chat'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth,
        chat
    },
    /**
     * If strict mode should be enabled.
     */
    strict: debug,
    devtools: debug,
    /**
     * Plugins used in the store.
     */
    plugins: debug ? [createLogger()] : []
})
