/* ============
 * vee-validate
 * ============
 *
 * vee-validate is a template-based validation framework for Vue.js that allows you to validate inputs and display
 * errors
 *
 * https://github.com/logaretm/vee-validate
 */

import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import * as VeeValidate from 'vee-validate'
import {
  required,
  email,
  numeric,
  regex,
  confirmed,
  length,
} from 'vee-validate/dist/rules'
import ru from 'vee-validate/dist/locale/ru'

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

extend('required', required)
extend('email', email)
extend('confirmed', confirmed)
extend('numeric', numeric)
extend('regex', regex)
extend('length', length)

VeeValidate.localize('ru', ru)
