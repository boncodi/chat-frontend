import Vue from "vue"
import axios from 'axios'
import { API_URL } from '@/constants/api'
import store from '@/store'
import router from '@/router'

const INTERNAL_ERROR = 500
const NOT_FOUND = 404
const UNAUTHORIZED = 401
const FORBIDDEN = 403


const errorInterceptors = async error => {
    const status = error?.response?.status

    if (status === INTERNAL_ERROR) {
        return Vue.$toast.error('Internal server error')
    }

    if (status === NOT_FOUND) {
        return Vue.$toast.error('Not found')
    }

    if (status === UNAUTHORIZED || FORBIDDEN === status) {
        await store.dispatch('auth/logout')
        return router.push({
            name: 'auth-login'
        })
    }

    if (!status) {
        return Vue.$toast.error('Error connecting to the internet')
    }

    return Promise.reject(error)
}

axios.defaults.baseURL = API_URL

axios.interceptors.request.use(request => {
    const token = localStorage.getItem('token')
    if (token) {
        request.headers.common.Authorization = `Bearer ${token}`
    }
    return request
})

axios.interceptors.response.use(
    response => response,
    errorInterceptors
)

Vue.prototype.$axios = axios

export default axios
