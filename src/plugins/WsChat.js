import {WS_URL} from "@/constants/api";

class WsChat extends WebSocket {

    constructor(roomUuid, receiptId) {
        const token = localStorage.getItem('token')
        super(`${WS_URL}/chat/ws/${roomUuid}/${receiptId}?token=${token}`);
    }
}

export default WsChat